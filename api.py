from flask import Flask, request, jsonify, make_response
import hashlib
import uuid
import datetime
import sys
import dbHandler

app = Flask("TODOAPI")

def generate_password_hash(password):
	hash = hashlib.md5(password.encode())
	print(hash.hexdigest(), flush=True)
	return hash.hexdigest()

def check_password_hash(user, password):
	hash = hashlib.md5(password.encode()).hexdigest()
	if hash == user.password:
		return True
	return False

def getInitialClientIP(request):
	print("trying to get IP", flush=True)
	ip = None

	if(request.data):
		if request.is_json:
			data = request.get_json()
			if 'ip' in data:
				ip = data['ip']
		else:
			print("not in json", flush=True)

	if ip == None: #No Ip found in the request json body, so the request should come directly from client.
		ip = request.remote_addr
	return ip


@app.route('/')
def hello():
	rep = dict()
	rep['message'] = "Welcome to the task API."
	ip = getInitialClientIP(request)
	rep['ip'] = ip

	print(str(rep), flush=True)
	return rep

@app.route('/user', methods=['GET'])
def get_all_users():
	#Should only be available to admin
	print("get all user route", flush=True)
	output = dbHandler.get_all_users()
	return jsonify({'users': output})

@app.route('/user/<p_id>', methods=['GET'])
def get_one_user(p_id):
	#Should only be available to admin and the actual user p_id

	return jsonify({'message' : 'route get one user is not implemented'})


@app.route('/user', methods=['POST'])
def create_user():
	#Should only be available to admin

	data = request.get_json()
	hashed_password = generate_password_hash(data['password'])
	dbHandler.create_user(str(uuid.uuid4()), data['name'], hashed_password, False)
	return jsonify({'message': 'new user created'})

@app.route('/user/<p_id>', methods=['PUT'])
def promote_user(p_id):
	#Should only be available to admin

	return jsonify({'message' : 'route promote user is not implemented'})


@app.route('/user/<p_id>', methods=['DELETE'])
def delete_user(p_id):
	#Should only be available to admin

	return jsonify({'message' : 'route delete user is not implemented'})


@app.route('/task', methods=['GET'])
def get_all_tasks():
	#Should only be available to users (not admins)

	output = dbHandler.get_all_tasks()
	return jsonify({'tasks':output})


@app.route('/task/<task_id>', methods=['GET'])
def get_one_task(task_id):
	#Should only be available to users (not admins)
	output = dbHandler.get_one_task(task_id)
	return jsonify({'task':output})


@app.route('/task', methods=['POST'])
def create_task():
	#Should only be available to users (not admins)
	current_username = "tempOwner"
	data = request.get_json()
	dbHandler.create_task(data['text'], current_username)

	return jsonify({'message': 'Task created for ' + current_username})


@app.route('/task/<task_id>', methods=['PUT'])
def complete_task(task_id):
	#Should only be available to users (not admins)
	return jsonify({'message' : 'route complete task is not implemented'})



@app.route('/task/<task_id>', methods=['DELETE'])
def delete_task(task_id):
	#Should only be available to users (not admins)

	return jsonify({'message' : 'route delete task is not implemented'})

@app.route('/login', methods=['post'])
def login():
	return jsonify({'message' : 'login route is not implemented'})


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5555)

